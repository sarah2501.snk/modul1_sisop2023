# Kelompok D11 - SISOP D 
## 5025211196 - Sandyatama Fransisna Nugraha
## 5025211105 - Sarah Nurhasna Khairunnisa
## 5025211148 - Katarina Inezita Prambudi




# Soal2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak `X` kali dengan X sebagai jam sekarang (ex: pukul `16:09` maka `X` nya adalah `16` dst. Apabila pukul `00:00` cukup download `1` gambar saja). Gambarnya didownload setiap `10 jam sekali` mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

1. File yang didownload memilki format nama `“perjalanan_NOMOR.FILE”` Untuk `NOMOR.FILE`, adalah urutan file yang download (`perjalanan_1`, `perjalanan_2`, dst)
File batch yang didownload akan dimasukkan ke dalam folder dengan format nama `“kumpulan_NOMOR.FOLDER”` dengan `NOMOR.FOLDER` adalah urutan folder saat dibuat `(kumpulan_1, kumpulan_2, dst)` 

2. Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap `1 hari` dengan nama zip `“devil_NOMOR ZIP”` dengan `NOMOR.ZIP` adalah urutan folder saat dibuat `(devil_1, devil_2, dst)`. Yang di ZIP hanyalah folder kumpulan dari soal di atas.

# Langkah yang dilakukan : 
## 1. Mencari angka jam saat ini
```R
#!/bin/bash

# Mengambil angka jam saat ini
Hour=$(date +%H)

if [ "$Hour" == "0" ]
then 
  img_total=1
else
  img_total=$Hour
fi
```

Ini digunakan untuk mendapatkan angka jam pada saat ini. Lalu menggunakan fungsi if else dengan fungsi apabila angka jam menunjukkan angka `0`, maka `img_total` akan menjadi `1` (`img_total` adalah variable yang digunakan untuk jumlah download gambar). Apabila angka jam menunjukkan angka `16`, maka akan mendownload gambar sebanyak `16` kali.

=====================================================================================
## 2. Download gambar berdasarkan dari jam saat ini

```R
# Membuat Function make_img untuk mendownload video
make_img(){
for ((i=1; i<= $img_total; i++))
do 
	wget -O "perjalanan_$i.jpg" "https://loremflickr.com/320/240/Indonesia"
done
}
```
Membuat sebuah function dengan nama make_img dengan fungsi mendownload img dari internet menggunakan fungsi wget. Lalu, nama file diganti menjadi `perjalanan_i` dengan i adalah total angka yang didownload secara berurutan.

=====================================================================================
## 3. Membuat function dengan menzip folder yang telah dibuat

```R
# Membuat function make_zip untuk menzip video
make_zip(){
i=1

while [ -d "kumpulan_$i" ]
do
mkdir "devil_$i"
zip -r "devil_$i.zip" "kumpulan_$i"
rm -r "devil_$i"
i=$((i + 1))
done 
}
```

Membuat sebuah function dengan nama make_zip dengan melakukan operasi while loop. While loop akan mengecek apakah ada folder dengan nama `kumpulan_$i`. Apabila terdapat folder dengan nama yang sama, akan membuat sebuah directory baru dengan nama `devil_$i`. Lalu akan menzipkan folder baru dengan fungsi `zip -r`. Selanjutnya akan menghapus folder dengan `nama devil_$i`. Operasi while akan berjalan dengan `i++`. Operasi while selesai 

=====================================================================================
## 5. Membuat function dengan download image
```R
dwn_img(){
i=1
while [ $i -lt $img_total ] 
do 
 if [ ! -d "kumpulan_$i" ] 
 then 
   mkdir "kumpulan_$i"
   cd kumpulan_$i
   make_img
 fi
i=$((i + 1)) 
done
}
```

Membuat sebuah function dengan nama dwn_img dengan fungsi memanggil function make_img dan membuat folder baru. Operasi ini dimulai dengan while loop dengan mengecek apakah ada folder dengan nama `kumpulan_$i`. Lalu bila folder tidak ada, maka akan membuat folder baru dengan nama `kumpulan_$i`. Selanjutnya akan mengarahkan ke directory `kumpulan_$i` dan memanggil fungsi make_img. Lalu operasi while akan terus berjalan dengan i++

=====================================================================================
## 6. Membuat function dengan menghapus folder yang lama

```R
delete_folder(){
i=1
while [ -d "kumpulan_$i" ] 
do 
 rm -r "kumpulan_$i"
i=$((i + 1)) 
done
}
```

Membuat sebuah function dengan fungsi menghapus folder yang lama demi menghemat penyimpanan. Function ini sama dengan yang diatas dengan perbedaan menggunaan rm -r untuk menghapus folder. 


## 7. Memulai program menu dengan menggunaakan while loop dan switch case. 

```R
while true 
do
echo "=== Soal 2 ==="
echo "Pilih opsi dibawah ini"
echo "1. Download"
echo "2. Zip"
echo "3. Delete Folder"
read -p "Masukkan jawaban : " pilihan
case "$pilihan" in
   "1") dwn_img
   ;;
   "2") make_zip
   ;; 
   "3") delete_folder
   ;; 
   *)   echo "PIlihan tidak valid"
   ;;
  esac
  
done
```

Membuat function dengan memanfaatkan switch case dan while loop sehingga program terus berjalan. User akan memasukkan pilihan-pilhan dalam menu untuk melakukan proses download,zip,hapus dan sebagainya. Ketika user memilih angka 1, maka akan memanggil fungsi dwn_img. Ketika user memilih angka 2, maka akan memanggil fungsi make_zip dan seterusnya. 



